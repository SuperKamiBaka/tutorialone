import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Alessandro';
  heading: string = 'This is our first tutorial';
  contentArray: any = [];

  addAllToArray(content: string) {
    this.contentArray.push(content);
    console.log(this.contentArray);
  }
}
